<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_kana');
            $table->string('nickname');
            $table->string('birthplace');
            $table->string('address');
            $table->string('other_group_belonging');
            $table->string('mail_addr')->unique();
            $table->string('line_id');
            $table->string('twitter_id');
            $table->string('skype_id');
            $table->string('join_year');
            $table->date('birth_date');
            $table->string('geneki_job');
            $table->string('obkai_job');
            $table->text('hobby');
            $table->text('like');
            $table->text('dislike');
            $table->string('current_job');
            $table->string('dojin_circle');
            $table->text('free_comment');
            $table->string('password');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->binary('icon');
            $table->boolean('pvt_birthplace');
            $table->boolean('pvt_address');
            $table->boolean('pvt_mail_addr');
            $table->boolean('pvt_line_id');
            $table->boolean('pvt_twitter_id');
            $table->boolean('pvt_skype_id');
            $table->boolean('pvt_birth_date');
            $table->boolean('pvt_current_job');
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
